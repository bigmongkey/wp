@extends('admin.layouts.app')
@section('main-content')
  <div class="row">
  <div class="col-lg-12">
    <h1 class="page-header">ประวัติโรงเรียน</h1> <a href=" {{action('Admins\HistoryController@create')}}" class=" add pull-right"><i class="fa fa-plus fa-3x "></i></a>
  </div>

    @foreach( $history as $h )
    <a class="btn btn-xs btn-warning" href="{{ route('admin.history.edit', $h->id) }}"><i class="glyphicon glyphicon-edit"></i> Edit</a>
    <form action="{{ route('admin.history.destroy', $h->id) }}" method="POST" style="display: inline;" onsubmit="if(confirm('Delete? Are you sure?')) { return true } else {return false };">
          <input type="hidden" name="_method" value="DELETE">
          <input type="hidden" name="_token" value="{{ csrf_token() }}">
          <button type="submit" class="btn btn-sm btn-danger btn-de "><i class="glyphicon glyphicon-trash"></i> Delete</button>
        </form>
        
      <h1>{{$h->title}}</h1>
      {!!$h->detail!!}
      @endforeach
    </div>
  @endsection