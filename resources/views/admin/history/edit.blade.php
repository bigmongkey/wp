@extends('admin.layouts.app')
@section('main-content')
  {{ Form::model($history, array('class' => 'form-horizontal', 'id'=> 'my-awesome-dropzone', 'method' => 'PATCH', 'route' => array('admin.history.update', $history->id), 'files' => true)) }}
<div class="form-group">
  {!! Form::label('title', 'หัวข้อ') !!}
  {{ Form::text('title',value($history->title) , array('class'=>'form-control', 'placeholder'=>'Enter Title Here')) }}
</div>

<div class="form-group">
  {!! Form::label('detail', 'รายละเอียด') !!}
  {{ Form::textarea('detail', null, array('id'=>'desc1','class' => 'ckeditor','value'=>'$history->detail')) }}
</div>

<div class="form-group">
  {!! Form::submit('Save', array( 'class'=>'btn btn-danger form-control' )) !!}
</div>

{!! Form::close() !!}

 @endsection