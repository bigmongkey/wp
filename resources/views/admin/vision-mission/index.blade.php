@extends('admin.layouts.app')
@section('main-content')
  <div class="row">
    <div class="col-lg-12">
      <h1 class="page-header">วิสัยทัศน์และพันธกิจ</h1> 
      <a href=" {{ url('admin/vision-mission/create')}}" class=" add pull-right"><i class="fa fa-plus fa-3x "></i></a>
    </div>
    @foreach( $vision as $v )
     <form action="{{ route('admin.vision-mission.destroy', $v->id) }}" method="POST" style="display: inline;" onsubmit="if(confirm('Delete? Are you sure?')) { return true } else {return false };">
          <input type="hidden" name="_method" value="DELETE">
          <input type="hidden" name="_token" value="{{ csrf_token() }}">
          <button type="submit" class="btn btn-sm btn-danger btn-de "><i class="glyphicon glyphicon-trash"></i> Delete</button>
        </form>

      <h1>{{$v->title}}</h1>
      {!!$v->body!!}
    @endforeach
  </div>


  @endsection