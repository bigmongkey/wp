@extends('admin.layouts.app')
@section('main-content')
  <div class="row">
    <div class="col-lg-12">
      <h1 class="page-header">พันธกิจ</h1>


      {!! Form::open(['action'=>'Admins\VisionMissionController@store', 'files'=>true]) !!}
      <div class="form-group">
        {!! Form::label('title', 'หัวข้อ') !!}
        {{ Form::text('title', null, array('class' => 'hi-title')) }}
      </div>

      <div class="form-group">
        {!! Form::label('dbody', 'รายละเอียด') !!}
        {{ Form::textarea('body', null, array('id'=>'desc1','class' => 'ckediter')) }}
      </div>

      <div class="form-group">
        {!! Form::submit('Save', array( 'class'=>'btn btn-danger form-control' )) !!}
      </div>

      {!! Form::close() !!}
    </div>
  </div>
@endsection