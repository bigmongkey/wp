@extends('admin.layouts.app')

@section('main-content')
<div class="container">
  <div class="row">
    <div class="col-md-10 col-md-offset-1">
      <img src="/uploads/avatars/{{ $user->avatar }}" style="width:150px; height:150px; float:left; border-radius:50%; margin-right:25px;">
      <h2> Profile Update</h2>
      {{ Form::model($user, array('class' => 'form-horizontal', 'id'=> 'my-awesome-dropzone', 'method' => 'PATCH', 'route' => array('admin.profile.update', $user->id), 'files' => true)) }}
      
      <div class="form-group">
      {!! Form::label('avatar', 'Choose an image') !!}
      {!! Form::file('avatar') !!}
    </div>

      <div class="form-group">
        {!! Form::label('name', 'name') !!}
        {{ Form::text('name',value($user->name) ,array('class'=>'form-control','placeholder'=>'Enter Title Here')) }}
      </div>

      <div class="form-group">
        {!! Form::label('email', 'email') !!}
        {{ Form::email('email', value($user->email),['class'=>'form-control']) }}
      </div>

      <div class="form-group">
        {!! Form::label('Phone Number', 'Phone Number') !!}
        {{ Form::text('tel', value ($user->tel),['class'=>'form-control']) }}
      </div>

      <div class="form-group">
        {!! Form::submit('Save', array( 'class'=>'btn btn-info form-control' )) !!}
      </div>

      {!! Form::close() !!}
    </div>
  </div>
</div>
@endsection