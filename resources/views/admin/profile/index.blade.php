@extends('admin.layouts.app')

@section('main-content')
<div class="container">
<div class="header"><h1 page-header>User Profile</h1></div>
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <img src="/uploads/avatars/{{ $user->avatar }}" style="width:150px; height:150px; float:left; border-radius:50%; margin-right:25px;"> 
             <a class="btn btn-xs btn-warning" href="{{ route('admin.profile.edit', $user->id) }}"><i class="glyphicon glyphicon-edit"></i> Edit</a>

             <a class="btn btn-xs btn-warning" href="{{ url('admin/password') }}"><i class="glyphicon glyphicon-edit"></i> Edit Password</a>

            @if ($user->is_admin == true)
            <h2> Profile Admin</h2>
           
            @endif
            <h4> Name: {{ $user->name }}</h4>
            <h4> Phone Number: {{$user->tel}}</h4>
            <h4>Email: {{ $user->email }}</h4>

        </div>
    </div>
</div>
@endsection