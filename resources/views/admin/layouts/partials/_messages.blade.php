@if (session('success'))
<div class="flash-m animated">
  <div class="alert alert-success" role="alert">
    <strong>Success: </strong>{{ session('success') }}
  </div>
</div>
@elseif(session('delete'))
<div class="flash-m animated">
  <div class="alert alert-warning role="alert">
    <stron>Success: </stron>{{  session('delete') }}
  </div>
</div>
@elseif(session('error'))
<div class="flash-m animated">
  <div class="alert alert-danger role="alert">
    <stron>Error: </stron>{{  session('error') }}
  </div>
</div>

@endif
