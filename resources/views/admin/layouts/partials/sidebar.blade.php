<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">

  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">

    <!-- Sidebar user panel (optional) -->
    @if (! Auth::guest())
    <div class="user-panel">
      <div class="pull-left image">
         <img src="/uploads/avatars/{{ Auth::user()->avatar }}" class="img-circle" alt="User Image" />
      </div>
      <div class="pull-left info">
        <p>{{ Auth::user()->name }}</p>
        <!-- Status -->
        <a href="#"><i class="fa fa-circle text-success"></i> {{ trans('adminlte_lang::message.online') }}</a>
      </div>
    </div>
    @endif

    <!-- search form (Optional) -->
    <form action="#" method="get" class="sidebar-form">
      <div class="input-group">
        <input type="text" name="q" class="form-control" placeholder="{{ trans('adminlte_lang::message.search') }}..."/>
        <span class="input-group-btn">
          <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i></button>
        </span>
      </div>
    </form>
    <!-- /.search form -->

    <!-- Sidebar Menu -->
    <ul class="sidebar-menu">
     <li>
      <a href="{{ url('admin') }}" ><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
    </li >
    <li >
      <a href="{{ url('admin/image-header') }}"><i class="fa fa-file-image-o"></i> รูปภาพหน้าหลัก</a>
    </li>
    <li class="treeview">
      <a href="#"><i class='fa fa-link'></i> <span>ข้อมูลโรงเรียน</span> <i class="fa fa-angle-left pull-right"></i></a>
      <ul class="treeview-menu">
        <li>
          <a href="{{url('admin/history')}}">ประวัติโรงเรียน</a>
        </li>
        <li>
          <a href="{{url('admin/vision-mission')}}">วิสัยทัศน์และพันธกิจ</a>
        </li>
        <li>
          <a href="morris.html">สัญสักษณ์โรงเรียน</a>
        </li>

        <li>
          <a href="morris.html">ทำเนียบผู้บริหาร</a>
        </li>
        <li>
          <a href="morris.html">พื้นที่สถานศึกษา</a>
        </li>

        <li>
          <a href="morris.html">ข้อมูลนักเรียน</a>
        </li>
        <li>
          <a href="morris.html">ข้อมูลบุคลากร</a>
        </li>
        <li>
          <a href="morris.html">ข้อมูลคอมพิวเตอร์ และ อินเทอร์เน็ต</a>
        </li>
        <li>
          <a href="morris.html">ข้อมูลที่ดินสิ่งก่อสร้าง</a>
        </li>
        <li>
          <a href="morris.html">ข้อมูลครุภัณฑ์</a>
        </li>
      </ul>

    </li>
    <li>
      <a href="#"><i class="fa fa-street-view"></i> ข้อมูลบุคลากร<span class="fa arrow"></span></a>
      <ul class="nav nav-second-level">
        <li>
          <a href="flot.html">คณะกรรมการสถานศึกษา</a>
        </li>
        <li>
          <a href="morris.html">คณะกรรมการนักเรียน</a>
        </li>
      </ul>

    </li> 

    <li>
      <a href="index.html"><i class="fa fa-newspaper-o"></i> ข่าวประชาสัมพันธ์</a>
    </li> 
    <li>
      <a href="index.html"><i class="fa fa-trophy"></i> ผลงานนักเรียน</a>
    </li>

    <li>
      <a href="index.html"><i class="fa fa-picture-o"></i> ภาพกิจกรรม </a>
    </li>


    <li>
      <a href="index.html"><i class="fa fa-phone-square"></i> ติดต่อสอบถาม</a>
    </li> 

  </ul><!-- /.sidebar-menu -->
</section>
<!-- /.sidebar -->
</aside>
