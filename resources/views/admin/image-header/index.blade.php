@extends('admin.layouts.app')
@section('main-content')
  <div class="row">
    <div class="col-lg-12">
      <h1 class="page-header">ภาพหัวเว็บ</h1> <a href=" {{action('Admins\ImageController@create')}}" class=" add pull-right"><i class="fa fa-plus fa-3x "></i></a>
    </div>
    @foreach( $images as $image )
      <div class="row">

        <form action="{{ route('admin.image-header.destroy', $image->id) }}" method="POST" style="display: inline;" onsubmit="if(confirm('Delete? Are you sure?')) { return true } else {return false };">
          <input type="hidden" name="_method" value="DELETE">
          <input type="hidden" name="_token" value="{{ csrf_token() }}">
          <button type="submit" class="btn btn-sm btn-danger btn-de "><i class="glyphicon glyphicon-trash"></i> Delete</button>
        </form>

        <div class="row header-img">
          <a href="{!! '/images/'.$image->fileName !!}">
            <img src="{!!  asset('images/' . $image->fileName) !!}"  class="img-thumbnail" >
          </a>
        </div>
      </div>
    @endforeach
    <?php echo $images->render(); ?>:
  </div>
@endsection