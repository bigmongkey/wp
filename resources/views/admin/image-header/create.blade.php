@extends('admin.layouts.app')
@section('main-content')
  <div class="row">
    <div class="col-lg-12">
      <h1 class="page-header">ภาพหัวเว็บ</h1>
    </div>

    {!! Form::open(['action'=>'Admins\ImageController@store', 'files'=>true]) !!}

    <div class="form-group">
      {!! Form::label('image', 'Choose an image') !!}
      {!! Form::file('image') !!}
    </div>

    <div class="form-group">
      {!! Form::submit('Save', array( 'class'=>'btn btn-danger form-control' )) !!}
    </div>

    {!! Form::close() !!}
    <div class="alert-warning">
      @foreach( $errors->all() as $error )
        <br> {{ $error }}
      @endforeach
    </div>

  </div>
@endsection