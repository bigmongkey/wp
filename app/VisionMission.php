<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VisionMission extends Model
{
  protected $table = 'vision-mission';
}
