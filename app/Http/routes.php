<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::group(['prefix' => 'admin', 'middleware' => 'admin' ],function () {
	Route::get('password', 'Admins\PasswordController@password');
	Route::post('updatepassword', 'Admins\PasswordController@updatePassword');
	Route::resource('profile','Admins\ProfilesController', ['only' => [
		'index', 'edit','update'
		]]);
	Route::resource('vision-mission','Admins\VisionMissionController');
	Route::resource('image-header', 'Admins\ImageController' );
	Route::resource('history' , 'Admins\HistoryController');
	Route::controller('/','Admins\DashboardController');


});

Route::group(['middleware' => 'web'],function (){
	Route::get('/',function () {
		return view('home/index');
	});
	Route::auth();

});