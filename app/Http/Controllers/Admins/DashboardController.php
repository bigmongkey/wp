<?php

namespace App\Http\Controllers\Admins;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class DashboardController extends Controller
{
    public function getIndex(){
      /*  if(Auth::check()){
            redirect('admin/');
        }
        else{
            return ('welcome');
        }
      */
    	return view('admin.dashboard.index');
    }
}
