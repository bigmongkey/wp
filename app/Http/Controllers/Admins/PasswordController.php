<?php

namespace App\Http\Controllers\Admins;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Hash;
use Auth;
use Session;
use App\User;

class PasswordController extends Controller
{
	public function password(){
		return View('admin.password');
	}
	public function updatePassword(Request $request){
		$rules = [
		'mypassword' => 'required',
		'password' => 'required|confirmed|min:6|max:18',
		];

		$messages = [
		'mypassword.required' => 'please fill out this field',
		'password.required' => 'please fill out this field',
		'password.confirmed' => "Password doesn't match the confirmation and Old password isn't valid",
		'password.min' => 'The minimum allowed is 6 characters',
		'password.max' => 'The maximum allowed is 18 characters',
		];

		$validator = Validator::make($request->all(), $rules, $messages);
		if ($validator->fails()){
			Session::flash('error','Please input infomations');
			return redirect('admin/password')->withErrors($validator);
		}
		else{
			if (Hash::check($request->mypassword, Auth::user()->password)){
				$user = new User;
				$user->where('email', '=', Auth::user()->email)
				->update(['password' => bcrypt($request->password)]);
				Session::flash('error','Password update successfully');
				return redirect('admin');
			}
			else
			{
				Session::flash('error','incorrect credentials');
				return redirect('admin/password');
			}
		}
	}
}
