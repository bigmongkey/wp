<?php

namespace App\Http\Controllers\Admins;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use App\User;
use Image;
use Session;
use Hash;

class ProfilesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()

    {       

        return view('admin.profile.index', array('user' => Auth::user()) );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::findOrFail($id);
        return view('admin.profile.edit',compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
     $rules = [
     'mypassword' => 'required',
     'password' => 'required|confirmed|min:6|max:18',
     ];

     $messages = [
     'mypassword.required' => 'El campo es requerido',
     'password.required' => 'El campo es requerido',
     'password.confirmed' => 'Los passwords no coinciden',
     'password.min' => 'El mínimo permitido son 6 caracteres',
     'password.max' => 'El máximo permitido son 18 caracteres',
     ];
     if($request->hasFile('avatar' )){
      $avatar = $request->file('avatar');
      $filename = time() . '.' . $avatar->getClientOriginalExtension();
      Image::make($avatar)->resize(300, 300)->save( public_path('/uploads/avatars/' . $filename ) );
      $user = Auth::user();
      $user->name = $request->input("name");
      $user->tel = $request->input("tel");
      $user->email = $request->input("email");
      $user->avatar = $filename;

      $user->save();
  }else
  {
    $user = Auth::user();
    $user->name = $request->input("name");
    $user->email = $request->input("email");
    $user->tel = $request->input("tel");
    $user->save();

}
Session::flash('success','update successfully save!');
return redirect()->route('admin.profile.index');

}

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
